import pandas as pd
import json
import pymysql
import sys
import numpy as np
from datetime import datetime
import time
import smtplib, ssl
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import os
import datetime

class Mail:

    ''' Sends mail from 'raja@zestiot.io' to specified users '''

    def send_mail(self, text, subject, toaddress):
        msg = MIMEMultipart()
        msg['From'] = 'raja@zestiot.io'
        msg['To'] = ",".join(['aayush@zestiot.io', 'nilesh@zestiot.io', 'aman@zestiot.io', 'amit@zestiot.io', 'samson@zestiot.io', 'anwesh@zestiot.io', 'sushanto@zestiot.io', 'arshiya@zestiot.io', 'nikhileshwar@zestiot.io', 'anil@zestiot.io', 'krishna@zestiot.io', 'chiranjeevi@zestiot.io', 'rchiluka@enhops.com', 'niharika@zestiot.io'])
        msg['Subject'] = subject
        msg.attach(MIMEText(text, 'html'))
        try:
            mailserver = smtplib.SMTP('smtp.office365.com',587)
            mailserver.ehlo()
            mailserver.starttls()
            mailserver.login('raja@zestiot.io', 'Raza@2523')
            mailserver.sendmail('raja@zestiot.io', toaddress, msg.as_string())
        except:
            print("Error", sys.exc_info()[0])
        mailserver.quit()


class Query_Execution:

    ''' Executes query with database connection as per config file '''

    def __init__(self, configfile):
        config = json.loads(open(configfile).read())
        try:
            self.db = pymysql.connect(host=config["_mysql"]["host"], user=config["_mysql"]["user"], passwd=config["_mysql"]["passwd"], db=config["_mysql"]["db"])
        except Exception as e:
            print(e)
    def query_execute(self, sqlquery):
        return pd.read_sql(sqlquery, con=self.db)


class DataPre:

    ''' contains prerequisite data needed '''

    op = {    "ADSB" : [], "BBA" : [], "Cam" : [], "LAD" : [["LAP","LAA"],["LAP"]],             "BFL" : ["BFA","BFP", "BFU", "BFL"], "COA" : [['PCA'], ["PCBA","PCB"],["PCDA","PCD"]] , "CAT" : ["CAA","CAT"], "PBT" : ["PBT", "PushBack"], "WCART" : ["WFG"], "TCART" : ["TCG"], "ETT" : ["ETT"], "ACU" : ["ACU"], "ASU" : ["ASU"], "GPU" : ["GPU"], "FUEL" : ["FLE"], "SCHE": ["CHO"], "BAY" : ["CHO"], "CAR" : ["CAR"], "FL" : ["NO ACT"], "TR" : ["NO ACT"]}
    lo = {    "CAR" : ["EquipActivityLogs", "GPSLogs", "IgnitionLogs"], "TR" : ["EquipActivityLogs", "GPSLogs", "IgnitionLogs"], "ASU" : ["EquipActivityLogs", "GPSLogs", "IgnitionLogs"], "FL" : ["EquipActivityLogs", "GPSLogs", "IgnitionLogs"], "LAD" : [["EquipActivityLogs", "GPSLogs", "IgnitionLogs", "SonarLogs"],["LadderLogs"]] , "BFL" : ["EquipActivityLogs", "MagneticSensorLogs", "GPSLogs", "IgnitionLogs", "SonarLogs"], "COA" : ["EquipActivityLogs", "GPSLogs", "IgnitionLogs"], "CAT" : ["EquipActivityLogs", "GPSLogs", "IgnitionLogs", "SonarLogs"], "PBT" : ["EquipActivityLogs", "GPSLogs", "IgnitionLogs"], "WCART" : ["EquipActivityLogs", "GPSLogs", "IgnitionLogs"], "TCART" : ["EquipActivityLogs", "GPSLogs", "IgnitionLogs"], "ETT" : ["EquipActivityLogs", "GPSLogs", "IgnitionLogs", "BeaconAlerts"], "ACU" : ["EquipActivityLogs", "GPSLogs", "IgnitionLogs"], "GPU" : ["EquipActivityLogs", "GPSLogs", "IgnitionLogs"], "FUEL" : ["EquipActivityLogs", "GPSLogs", "IgnitionLogs"], "BBA" : ["EquipActivityLogs", "BeaconAlerts"], "SCHE" : ["EquipActivityLogs"], "BAY" : ["EquipActivityLogs", "SoundLogs"], "ADSB" : ["EquipActivityLogs", "FlightMovement"], "Cam" : ["BayImages"]}
    logs = [ "MagneticSensorLogs", "SonarLogs", "IgnitionLogs", "BeaconAlerts", "GPSLogs", "EquipActivityLogs", "LadderLogs"]
    sensor_logs = ["EquipActivityLogs", "FlightMovement", "SoundLogs", "BayImages", "BeaconAlerts"]
    html_style = ''' <html>
                        <head>
                            <style>
                            table, th, td     {
                                                  border: 1px solid black;
                                                  border-collapse: collapse;
                                            }
                            th, td     {
                                          padding: 5px;
                                          text-align: center;    
                                    }
                            </style>
                        </head>
                        <body> '''
    date_column = {"GPSLogs" : 'LogDate', "BeaconAlerts" : 'LogDate', "IgnitionLogs" : 'LogDate', "SonarLogs" : 'LogDate', "MagneticSensorLogs" : 'LogDate', "EquipActivityLogs" : 'FLogDate', "BayImages" : 'LogDate', "FlightMovement" : 'Timestamp', "SoundLogs" : 'LogDate', 'LadderLogs' : 'LogDate_GPS'}
    color_dict_heading = {"total" : '#e67e22', 'CAA':'#fdfefe', 'LAP':'#fdfefe', 'CHO':'#fdfefe', 'ACU':'#fdfefe', 'ACL':'#fdfefe', 'GPU':'#fdfefe', 'CHF':'#fdfefe', 'BFL':'#fdfefe', 'BFU':'#fdfefe', 'CAT':'#fdfefe', 'LAA':'#fdfefe', 'PCG':'#fdfefe', 'PCDA':'#fdfefe', 'PCD':'#fdfefe', 'PCB':'#fdfefe', 'PCBA':'#fdfefe', 'FTD':'#fdfefe', 'LTD':'#fdfefe', 'ETT':'#fdfefe', 'FLE':'#fdfefe', 'PCA':'#fdfefe', 'PushBack':'#fdfefe', 'BBL':'#fdfefe', 'WFG':'#fdfefe', 'PBT':'#fdfefe', 'TCG':'#fdfefe', 'BBF':'#fdfefe', 'BEACONS':'#fdfefe'}
    color_dict_data = {"total" : '#f0b27a', 'CAA':'#fdfefe', 'LAP':'#fdfefe', 'CHO':'#fdfefe', 'ACU':'#fdfefe', 'ACL':'#fdfefe', 'GPU':'#fdfefe', 'CHF':'#fdfefe', 'BFL':'#fdfefe', 'BFU':'#fdfefe', 'CAT':'#fdfefe', 'LAA':'#fdfefe', 'PCG':'#fdfefe', 'PCDA':'#fdfefe', 'PCD':'#fdfefe', 'PCB':'#fdfefe', 'PCBA':'#fdfefe', 'FTD':'#fdfefe', 'LTD':'#fdfefe', 'ETT':'#fdfefe', 'FLE':'#fdfefe', 'PCA':'#fdfefe', 'PushBack':'#fdfefe', 'BBL':'#fdfefe', 'WFG':'#fdfefe', 'PBT':'#fdfefe', 'TCG':'#fdfefe', 'BBF':'#fdfefe', 'BEACONS':'#fdfefe'}
    color_value = '#85c1e9'

    @staticmethod
    def read_masterdata(file):
        old = open(file, 'r')
        battery_mac = []
        st_mac = old.readline()[:-1]
        while st_mac != 'End':
            st_mac = old.readline()[:-1]
            if len(st_mac) >= 12 and len(st_mac.split(':')) >= 6:
                mac = ''
                for mac_part in st_mac.split(':')[:6]:
                    for char in mac_part:
                        if char.isdigit():
                            mac = mac + str(char)
                    mac = mac + ':'
                battery_mac.append(mac[:-1])
        old.close()
        return battery_mac


class extra(DataPre):

    ''' Creates json objects with dummy data and completes the json using actual data '''
    ''' Sensor_json() will create a json object for all sensor devices in different airports as follows 
    airport {
                 Airport 1 : { 
                                Vendor 1 :    {
                                                Type 1 :DevID Log1 Log2 Log3 Flights_served 
                                                        Dev1   0     0     0     
                                                        Dev2   0     0     0     
                                                        Dev3   0     0     0
                                                Type 2 :DevID Log1 Log2 Log3 Flights_served
                                                        Dev1   0     0     0     
                                                        Dev2   0     0     0     
                                                        Dev3   0     0     0
                                             }
                                Vendor 2 :    {
                                                Type 1 :DevID Log1 Log2 Log3 Flights_served
                                                        Dev1   0     0     0     
                                                        Dev2   0     0     0     
                                                        Dev3   0     0     0
                                                Type 2 :DevID Log1 Log2 Log3 Flights_served
                                                        Dev1   0     0     0     
                                                        Dev2   0     0     0     
                                                        Dev3   0     0     0
                                             }
                             }
                 Airport 2 : { 
                                Vendor 1 :    {
                                                Type 1 :DevID Log1 Log2 Log3 Flights_served
                                                        Dev1   0     0     0     
                                                        Dev2   0     0     0     
                                                        Dev3   0     0     0
                                                Type 2 :DevID Log1 Log2 Log3 Flights_served
                                                        Dev1   0     0     0     
                                                        Dev2   0     0     0     
                                                        Dev3   0     0     0
                                             }
                                Vendor 2 :    {
                                                Type 1 :DevID Log1 Log2 Log3 Flights_served
                                                        Dev1   0     0     0     
                                                        Dev2   0     0     0     
                                                        Dev3   0     0     0
                                                Type 2 :DevID Log1 Log2 Log3 Flights_served
                                                        Dev1   0     0     0     
                                                        Dev2   0     0     0     
                                                        Dev3   0     0     0
                                             }
                             }

            }'''
    ''' device_json() will create json object which is similar to one created by sensor_json() but creates different json for different airport '''
    ''' device_logs() will retun health status of device by validating json from sendor_json() and device_json() data with one defined in DataPre '''
    ''' device_operations() will retun health status of device by validating json from sendor_json() and device_json() data with one defined in DataPre '''

    @staticmethod
    def sensor_json(df):
        airports = ['RGI', 'DIAL', 'BIAL']
        airport = {}
        for iairport in airports:
            devices = pd.Series(filter(lambda x : x if x.split('_')[0] == iairport else None, df)).dropna()
            device_types = devices.apply(lambda x : x.split('_')[-2][1:]).unique()
            device_type = {}
            for idevice_type in device_types:
                di = {}
                devices_type = pd.Series(filter(lambda x : x if x.split('_')[-2][1:] == idevice_type else None, devices)).dropna()
                di['Device Id'] = devices_type
                for log in DataPre.lo[idevice_type]:
                    if log == 'EquipActivityLogs':
                        di[log] = ["" for x in range(len(di['Device Id']))]
                    else:
                        di[log] = [0 for x in range(len(di['Device Id']))]
                di['Flights Served'] = [[] for x in range(len(di['Device Id']))]
                device_type[idevice_type] = pd.DataFrame(di)
            airport[iairport] = device_type
        return airport

    @staticmethod
    def device_json(df):
        vendors = df.apply(lambda x : x.split('_')[0] if x.split('_')[0] not in ['RGI', 'BIAL', 'DIAL'] else None).dropna().unique()
        vendor = {}
        for ivendor in vendors:
            vendor_devices = pd.Series(filter(lambda x : x if x.split('_')[0] == ivendor else None, df)).dropna()
            device_types = vendor_devices.apply(lambda x : x.split('_')[2][1:]).unique()
            device_type = {}
            for idevice_type in device_types:
                device_type_devices = pd.Series(filter(lambda x : x if x.split('_')[2][1:] == idevice_type else None, vendor_devices)).dropna()
                di = {'Device Id' : device_type_devices}
                device_type_length = len(di['Device Id'])
                logs_all = []
                list(map(lambda x : logs_all.extend(x) if isinstance(x, list) else logs_all.append(x), DataPre.lo[idevice_type]))
                for log in logs_all:
                    if log != 'EquipActivityLogs':
                        di[log] = [0 for i in range(device_type_length)]
                    else:
                        di[log] = ['' for i in range(device_type_length)]
                di['Flights Served'] = [[] for i in range(device_type_length)]
                device_type[idevice_type] = pd.DataFrame(di)
            vendor[ivendor] = device_type
        return vendor

    @staticmethod
    def device_logs(li):
        st =  ['RGI', 'DIAL', 'BIAL']
        tk = 0
        airport = {}
        for iairport in li:
            vendor = {}
            for ivendor in iairport:
                device_type = {}
                for idevice_type in [x for x in iairport[ivendor] if x != iairport]:
                    working = 0
                    down = 0
                    required_logs = []
                    if isinstance(DataPre.lo[idevice_type][0], list):
                        required_logs = [[x for x in y if x != 'EquipActivityLogs'] for y in DataPre.lo[idevice_type]]
                    else:
                        required_logs = [x for x in DataPre.lo[idevice_type] if x != 'EquipActivityLogs']
                    for i, row in iairport[ivendor][idevice_type].iterrows():
                        if not isinstance(required_logs[0], list):
                            list_log = [True if int(row[x]) > 0 else False for x in required_logs]
                            if not all(list_log) and any(list_log):
                                print(row['Device Id']+' missing ',end='')
                                li = [row['Device Id']]
                                for index in range(len(list_log)):
                                    if not list_log[index]:
                                        print(' '+required_logs[index],end = '')
                                        li.append(required_logs[index])
                                print()
                                TAP.append(li)
                            elif not any(list_log):
                                li = [row['Device Id'], 'No Logs']
                                TAP.append(li)
                            else:
                                li = [row['Device Id'], 'Working']
                                Trails_plan.append(li)
                        else:
                            list_log = []
                            for index in range(len(required_logs)):
                                list_log.append([True if int(row[x]) > 0 else False for x in required_logs[index]])
                            if any([all(x) for x in list_log]):
                                li = [row['Device Id'], 'Working']
                                Trails_plan.append(li)
                            else:
                                if any([any(x) for x in list_log]):
                                    for status_index in range(len(list_log)):
                                        if any(list_log[status_index]):
                                            print(row['Device Id']+' missing ',end='')
                                            li = [row['Device Id']]
                                            for index in range(len(list_log[status_index])):
                                                if not list_log[status_index][index]:
                                                    print(' '+required_logs[status_index][index],end = '')
                                                    li.append(required_logs[status_index][index])
                                            print()
                                            TAP.append(li)
                                else:
                                    li = [row['Device Id'], 'No Logs']
                                    TAP.append(li)
                        if isinstance(required_logs[0], list):
                            if any(map(lambda x : all(map(lambda y : True if row[y] != 0 else False, x)), required_logs)):
                                working += 1
                            else:
                                down += 1
                        else:
                            if all(map(lambda x : True if row[x] !=0 else False, required_logs)):
                                working += 1
                            else:
                                down += 1
                    device_type[idevice_type] = [working, down]
                vendor[ivendor] = device_type
            airport[st[tk]] = vendor
            tk += 1
        return airport

    @staticmethod
    def device_operations(li):
        TAP_act = {}
        AL = {}
        st =  ['RGI', 'DIAL', 'BIAL']
        ii = 0
        for airport in li:
            vi = {}
            for vendor in airport:
                dit = {}
                for dtype in airport[vendor]:
                    working = 0
                    down = 0
                    for index, row in airport[vendor][dtype].iterrows():
                        missing_logs = ''    
                        activity = []
                        activities = []
                        flag = False
                        if 'EquipActivityLogs' in list(airport[vendor][dtype].columns):
                            for x in [y.split('|')[1:-1] for y in row['EquipActivityLogs'].split('!')[1:]]:
                                activities.extend(x)
                            for x in activities:
                                if x not in activity:
                                    activity.append(x)
                            del activities
                        elif 'FlightMovement' in list(airport[vendor][dtype].columns):
                            pass

                        if dtype == 'BBA':
                            flag = any([lambda x : len(x) == 17 for x in activity])

                        else:
                            if len(DataPre.op[dtype]) != 0 and isinstance(DataPre.op[dtype][0], list):
                                for sublist in DataPre.op[dtype]:
                                    if set(sublist).issubset(set(activity)):
                                        flag = True
                                li = []
                                for sublist in DataPre.op[dtype]:
                                    li = li + sublist
                                missing_logs = ','.join(list(set(li) - set(activity)))    
                            else:
                                if set(DataPre.op[dtype]).issubset(set(activity)):
                                    flag = True
                                elif len(set(DataPre.op[dtype])) == 0:
                                    flag = True
                                missing_logs = ','.join(list(set(DataPre.op[dtype]) - set(activity)))
                        
                        if flag:
                            working += 1
                            TAP_act[row['Device Id']] = [missing_logs, 'Working']
                        else:
                            down += 1
                            TAP_act[row['Device Id']] = [missing_logs, 'Down']
                    dit[dtype] = [working+down, working]
                vi[vendor] = dit
            AL[st[ii]] = vi
            ii += 1
        return AL, TAP_act


class To_CSV(DataPre):

    ''' Converts the jsons created in class extra to csv file '''

    def device_operations_to_csv(self, st, Airport):
        fo = open(''+st+'_AS_HealthCheck.csv', 'w')
        fo.write('Airport,'+'Entity,'+'Equipment,'+'DevId,'+'Flight No,'+'Operations'+',,,'+ '\n\r')
        fo.write(st+ '\n\r')
        for i in Airport:
            fo.write(','+i+',,,'+ '\n\r')
            for j in Airport[i]:
                fo.write(',,'+j+',,'+'\n\r')
                for l, k in Airport[i][j].iterrows():
                    fo.write(',,,'+k['Device Id']+','+'\n\r')
                    if 'EquipActivityLogs' in list(Airport[i][j].columns):
                        fli = k['EquipActivityLogs'].split('!')[1:]
                        for op in fli:
                            operlist = op.split('|')[:-1]
                            fo.write(',,,,'+operlist[0]+','+'|'.join(operlist[1:])+'\n\r')
                    elif 'FlightMovement' in list(Airport[i][j].columns):
                        print(list(Airport[i][j].columns))
                        print(type(k['Flights Served']))
                        fli = list(pd.Series(k['Flights Served']).unique())
                        for op in fli:
                            fo.write(',,,,'+op+'\n\r')
        fo.close()

    def device_logs_to_csv(self, st, Airport):
        fo = open(''+st+'_DS_HealthCheckLo.csv', 'w')
        fo.write('Airport,'+'Entity,'+'Equipment,'+'DevId,'+ '\n\r')
        fo.write(st+ '\n\r')
        for i in Airport:
            fo.write(','+i+',,,'+ '\n\r')
            
            for j in Airport[i]:
                lg = []
                for l, k in Airport[i][j].iterrows():
                    if l == 0:
                        st = 'DevId,'
                        for logs1 in DataPre.lo[k['Device Id'].split('_')[-2][1:]]:
                            if type(logs1) is list:
                                for lll in logs1:
                                    if lll not in lg:
                                        lg.append(lll)
                            else:
                                lg.append(logs1)
                        for logs1 in lg:
                            if logs1 != 'EquipActivityLogs':
                                st = st + logs1 + ','
                        fo.write(',,' + j + ',' + st + '\n\r')
                    st = ''
                    for logs1 in lg:
                        if logs1 != 'EquipActivityLogs':
                            st = st + str(k[logs1]) + ','
                    fo.write(',,,'+k['Device Id']+','+ st +','+'\n\r')
        fo.close()


class Presentation(DataPre):

    @staticmethod
    def pivot_presentation(table):

        def row_to_html(idx, row):
            st = ''' <Tr> <Td>''' + idx + ''' </Td>'''
            for index, count in row.iteritems():
                if int(count) != 0 and index != 'total':
                    st = st + '''<Td bgcolor = '''+ DataPre.color_value +'''> {} </Td>'''.format(count)
                else:
                    st = st + '''<Td bgcolor = '''+ DataPre.color_dict_data[index] +'''> {} </Td>'''.format(count)
            st = st + '''</Tr>'''
            return st
        st = ''
        for idx, row in table.iterrows():
            if st == '':
                st = ''' <Table> <Tr> <Th> Activities --> </Th>'''
                for index, count in row.iteritems():
                    st = st + '''<Th bgcolor = '''+ DataPre.color_dict_heading[index] +'''> {} </Th>'''.format(index)
                st = st + '''</Tr>'''
            st = st + row_to_html(idx, row)
            # print(st)
        return st + '''</Table>'''

    @staticmethod
    def legend_presentation(Legend):

    	st = ''' <Table> <Tr> <Th> Legend --> </Th>'''
    	for clo in list(Legend.columns):
    		st = st + '''<Th bgcolor = '#c0392b'> {} </Th>'''.format(index)
    	st = st + '''</Tr>'''

    	




# This block of code will create objects for above classes
data = DataPre()
db_production = Query_Execution('HC/Config/' + 'Config.json')
db_camera = Query_Execution('HC/Config/' + 'Configb.json')
json_to_csv = To_CSV()
mail_to_zestiot = Mail()
today = datetime.date.today()
yesterday = today - datetime.timedelta(days = 1)
daybeforeyesterday = yesterday - datetime.timedelta(days = 1)
daybeforedaybeforeyesterday = daybeforeyesterday - datetime.timedelta(days = 1)
print(today, yesterday, daybeforeyesterday, daybeforedaybeforeyesterday)

Legend = pd.read_csv('HC/Data/EB_Data/Legend.csv')


# This block of code will results in mail regarding flights and operations done on it.
operations_needed = ['CAA', 'LAP', 'CHO', 'ACU', 'ACL', 'GPU', 'CHF', 'BFL', 'BFU', 'CAT', 'LAA', 'PCG', 'PCDA', 'PCD', 'PCB', 'PCBA', 'FTD', 'LTD', 'ETT', 'FLE', 'PCA', 'PushBack', 'BBL', 'WFG', 'PBT', 'TCG', 'BBF']
flight_arrival_departure = db_production.query_execute("select LogID, ATA, Sensor_ATA, ATD, Sensor_ATD from DailyFlightSchedule_Merged where date(ATA) ='"+yesterday.isoformat()+"' or date(Sensor_ATA) ='"+yesterday.isoformat()+"' or date(ATD) = '"+yesterday.isoformat()+"' or date(Sensor_ATD) = '"+yesterday.isoformat()+"'")
flight_arrival_departure['ATA'] = flight_arrival_departure['ATA'].apply(lambda x : x.date())
flight_arrival_departure['ATD'] = flight_arrival_departure['ATD'].apply(lambda x : x.date())
flight_arrival_departure['Sensor_ATA'] = flight_arrival_departure['Sensor_ATA'].apply(lambda x : x.date())
flight_arrival_departure['Sensor_ATD'] = flight_arrival_departure['Sensor_ATD'].apply(lambda x : x.date())
flight_arrival_departure['LogID'] = flight_arrival_departure['LogID'].astype(str)
flight_operations = db_production.query_execute("select flight_pk, FlightNo, OperationName from EquipActivityLogs where date(FLogDate) = '"+yesterday.isoformat()+"' and DevId like '%SG_%'")
flight_operations['flight_pk'] = flight_operations['flight_pk'].astype(str).apply(lambda x : x[:-2])
flight_operations = flight_operations[flight_operations.flight_pk != 'n']
flight_operations = flight_operations[-flight_operations['OperationName'].str.contains(':')].reset_index(drop = True)
flight_operations = flight_operations[flight_operations.OperationName.isin(operations_needed)].reset_index(drop = True)
flight_merged = flight_arrival_departure.merge(flight_operations, how = 'inner', left_on = 'LogID', right_on = 'flight_pk')
del flight_arrival_departure
del flight_operations
flight_merged = flight_merged.drop(['LogID', 'flight_pk'], axis = 1)
flight_merged['count'] = [1 for x in range(flight_merged.shape[0])]
table = pd.pivot_table(flight_merged, values='count', index=['FlightNo'], columns=['OperationName'], aggfunc=np.sum).fillna(0).astype(int)
table['total'] = table.sum(axis = 1, skipna = True) 
del flight_merged
empty_activity = []
for index_operation in (set(operations_needed) - set(table.columns)):
    empty_activity.append(index_operation)
    table[index_operation] = [0 for x in range(table.shape[0])]
for index_empty_activity in empty_activity:
    data.color_dict_heading[index_empty_activity] = '#c0392b'
    data.color_dict_data[index_empty_activity] = ' #d98880'
html_text = Presentation.pivot_presentation(table)

for index_empty_activity in empty_activity:
    data.color_dict_heading[index_empty_activity] = '#fdfefe'
    data.color_dict_data[index_empty_activity] = '#fdfefe'
# html_text = table.to_html(justify = 'center')
del table
cc = ['samson@zestiot.io', 'anwesh@zestiot.io']
toaddress = ['aayush@zestiot.io', 'nilesh@zestiot.io', 'aman@zestiot.io', 'amit@zestiot.io', 'sushanto@zestiot.io', 'anil@zestiot.io', 'krishna@zestiot.io', 'chiranjeevi@zestiot.io', 'rchiluka@enhops.com', 'niharika@zestiot.io'] + cc
#toaddress = ['raja@zestiot.io']
mail_to_zestiot.send_mail(DataPre.html_style + ''' <h2>Flights and Operations captured for SG</h2>''' + html_text, 'Flight Details', toaddress)
del html_text

# This block of code will results in mail regarding flights and operations done on it.
operations_needed = ['CAA', 'LAP', 'CHO', 'ACU', 'ACL', 'GPU', 'CHF', 'BFL', 'BFU', 'CAT', 'LAA', 'PCG', 'PCDA', 'PCD', 'PCB', 'PCBA', 'FTD', 'LTD', 'ETT', 'FLE', 'PCA', 'PushBack', 'BBL', 'WFG', 'PBT', 'TCG', 'BBF', 'BEACONS']
flight_arrival_departure = db_production.query_execute("select LogID, ATA, Sensor_ATA, ATD, Sensor_ATD from DailyFlightSchedule_Merged where date(ATA) ='"+yesterday.isoformat()+"' or date(Sensor_ATA) ='"+yesterday.isoformat()+"' or date(ATD) = '"+yesterday.isoformat()+"' or date(Sensor_ATD) = '"+yesterday.isoformat()+"'")
flight_arrival_departure['ATA'] = flight_arrival_departure['ATA'].apply(lambda x : x.date())
flight_arrival_departure['ATD'] = flight_arrival_departure['ATD'].apply(lambda x : x.date())
flight_arrival_departure['Sensor_ATA'] = flight_arrival_departure['Sensor_ATA'].apply(lambda x : x.date())
flight_arrival_departure['Sensor_ATD'] = flight_arrival_departure['Sensor_ATD'].apply(lambda x : x.date())
flight_arrival_departure['LogID'] = flight_arrival_departure['LogID'].astype(str)
flight_operations = db_production.query_execute("select flight_pk, FlightNo, OperationName, DevId from EquipActivityLogs where date(FLogDate) = '"+yesterday.isoformat()+"' and DevId like '%DIAL_%'")
# flight_operations = flight_operations[flight_operations.DevId != 'DIAL_DIAL_ASCHE_0001']
flight_operations['flight_pk'] = flight_operations['flight_pk'].astype(str).apply(lambda x : x[:-2])
flight_operations['OperationName'] = flight_operations['OperationName'].astype(str)
flight_operations = flight_operations[flight_operations['OperationName'] != 'nan']
for dev in list([x for x in list(flight_operations['DevId'].unique()) if 'ABBA' in x ]):
    print(dev)
    bea = []
    for idx, row in flight_operations[flight_operations['DevId'] == dev].iterrows():
        if row['OperationName'].lower() not in bea:
            bea.append(row['OperationName'].lower())
            flight_operations['OperationName'].iloc[idx] = 'BEACONS'
            flight_operations['flight_pk'].iloc[idx] = 'NA'
flight_operations = flight_operations[flight_operations.flight_pk != 'n']
flight_operations = flight_operations[-flight_operations['OperationName'].str.contains(':')].reset_index(drop = True)
flight_operations = flight_operations[flight_operations.OperationName.isin(operations_needed)].reset_index(drop = True)
print(list(flight_operations['OperationName'].unique()))
print(flight_arrival_departure.shape)
flight_arrival_departure.loc[flight_arrival_departure.shape[0]] = ['NA', 'nan', 'nan', 'nan', 'nan']
print(flight_arrival_departure.shape)
flight_merged = flight_arrival_departure.merge(flight_operations, how = 'inner', left_on = 'LogID', right_on = 'flight_pk')
del flight_arrival_departure
del flight_operations
flight_merged = flight_merged.drop(['LogID', 'flight_pk'], axis = 1)
flight_merged['count'] = [1 for x in range(flight_merged.shape[0])]
table = pd.pivot_table(flight_merged, values='count', index=['DevId'], columns=['OperationName'], aggfunc=np.sum).fillna(0).astype(int)
table['total'] = table.sum(axis = 1, skipna = True)
del flight_merged
empty_activity = []
for index_operation in (set(operations_needed) - set(table.columns)):
    empty_activity.append(index_operation)
    table[index_operation] = [0 for x in range(table.shape[0])]
for index_empty_activity in empty_activity:
    data.color_dict_heading[index_empty_activity] = '#c0392b'
    data.color_dict_data[index_empty_activity] = ' #d98880'
html_text = Presentation.pivot_presentation(table)
# html_text = table.to_html(justify = 'center')
del table
print('zestiot')
cc = ['samson@zestiot.io', 'anwesh@zestiot.io']
toaddress = ['aayush@zestiot.io', 'nilesh@zestiot.io', 'aman@zestiot.io', 'amit@zestiot.io' , 'sushanto@zestiot.io', 'anil@zestiot.io', 'krishna@zestiot.io', 'chiranjeevi@zestiot.io', 'rchiluka@enhops.com', 'niharika@zestiot.io'] + cc
#toaddress = ['raja@zestiot.io']
mail_to_zestiot.send_mail(DataPre.html_style + ''' <h2>Flights and Operations captured for DIAL</h2>''' + html_text, 'Flight Details', toaddress)
del html_text

# This block of code will get health status of devices and send a mail as summary
without_battery_mac = DataPre.read_masterdata('HC/Data/old.txt') # Contains mac address of devices without battery collected from master data.
with_battery_mac = DataPre.read_masterdata('HC/Data/New.txt') # Contains mac address of devices with battery  collected from master data.
ota_devid = []
#print('sucess')
df_ota = db_production.query_execute("select mac_addr, Configuration from OTA")
df_ota['mac_addr'] = df_ota['mac_addr'].str.strip()
df_ota = df_ota[~df_ota['Configuration'].str.contains('BC_GPSBT_')].reset_index()
df_ota_wb = df_ota[df_ota.mac_addr.isin(with_battery_mac)]
df_ota_wob = df_ota[df_ota.mac_addr.isin(without_battery_mac)]
df_ota_other = set(df_ota['mac_addr']) - set(df_ota_wb['mac_addr']) - set(df_ota_wob['mac_addr'])
print('other ',df_ota_other)
del df_ota
With_Battery = [z[3:].strip() for z in [y for x in list(df_ota_wb['Configuration']) for y in x.split(',')] if z[:3] == 'id=']
del df_ota_wb
Without_Battery = [z[3:].strip() for z in [y for x in list(df_ota_wob['Configuration']) for y in x.split(',')] if z[:3] == 'id=']
del df_ota_wob
ota_devid.extend(With_Battery)
ota_devid.extend(Without_Battery)
ota_devid.extend(df_ota_other)
ota_devid.sort()

Equip = pd.read_excel('HC/Data/Equipment Details.xlsx', sheet_name = ['GMR', 'SG', 'BIAL', 'DIAL', 'SENSOR'])
RGI = extra.device_json(pd.Series([x for x in ota_devid if '_RGI_' in x]))
BIAL = extra.device_json(pd.Series([x for x in ota_devid if '_BIAL_' in x]))
DIAL = extra.device_json(pd.Series([x for x in ota_devid if '_DIAL_' in x]))
# del ota_devid
SENSOR = extra.sensor_json(Equip['SENSOR']['Device Id'].dropna())
del Equip
def raj(ai,st1):
  st1 = st1 +'    '
  for temp_ai in ai:
    print('    '+temp_ai,end='')
    if isinstance(ai[temp_ai], dict):
      print()
      raj(ai[temp_ai], st1)
    else:
      print(st1+str(ai[temp_ai].shape[0]))

for ai, st in zip([RGI,DIAL,BIAL], ['RGI','DIAL','BIAL']):
  print(st)
  st1 = '    '
  raj(ai,st1)

for log in data.sensor_logs:
  print(log)
  if log != 'EquipActivityLogs':
      df = pd.DataFrame()
      if log == 'BayImages':
        df = db_camera.query_execute("select DevId from "+log+" where date("+DataPre.date_column[log]+") >= '"+ yesterday.isoformat()+"' and date("+DataPre.date_column[log]+") < '"+ today.isoformat()+"'")
        df['DevId'] = df['DevId'].apply(lambda x : 'RGI_RGI_BCam_0001' if x == 'BCam1' else x)
        df['DevId'] = df['DevId'].apply(lambda x : 'RGI_RGI_BCam_0002' if x == 'BCam2' else x)
        df['DevId'] = df['DevId'].apply(lambda x : 'RGI_RGI_BCam_0003' if x == 'BCam3' else x)
        df['DevId'] = df['DevId'].apply(lambda x : 'RGI_RGI_BCam_0004' if x == 'BCam4' else x)
      elif log == 'FlightMovement':
        df = db_production.query_execute("select DevId, Flight from "+log+" where date("+DataPre.date_column[log]+") >= '"+yesterday.isoformat()+"' and date("+DataPre.date_column[log]+") < '"+ today.isoformat()+"'")
#        print(df['DevId'].unique())
        df['DevId'] = df['DevId'].apply(lambda x : 'RGI_RGI_AADSB_0001' if x == None else x)
        df['DevId'] = df['DevId'].apply(lambda x : 'RGI_RGI_AADSB_0001' if x == 'RGI_AADSB_0001' else x)
        df['DevId'] = df['DevId'].apply(lambda x : 'RGI_RGI_AADSB_0002' if x == 'RGI_AADSB_0002' else x)
        df['DevId'] = df['DevId'].apply(lambda x : 'RGI_RGI_AADSB_test' if x == 'ZT_AADSB_0006' else x)
 #       print(df['DevId'].unique())
        print(SENSOR)
        for dev in df['DevId'].unique():
          print(dev)
          if all([x in ["RGI", "DIAL", "BIAL", "AADSB"] for x in dev.split('_')[:3]]):
            li = list(SENSOR[dev.split('_')[0]][dev.split('_')[-2][1:]]['Device Id'])
            if dev in li:
              idd = list(SENSOR[dev.split('_')[0]][dev.split('_')[-2][1:]].columns).index('Flights Served')
              SENSOR[dev.split('_')[0]][dev.split('_')[-2][1:]].iloc[li.index(dev), idd].extend(list(df[df['DevId'] == dev]['Flight']))
            else:
              print("unknown device "+dev+ " in "+log)   
          else:
            print("1name error for "+dev+" in "+log)
      else:
        df = pd.DataFrame()
        if log == 'BeaconAlerts':
          dfC = pd.read_csv('HC/Data/EB_Data/'+ log + '@' + str(yesterday) + '.csv')
          dfD = pd.read_csv('HC/Data/EB_Data/'+ log + '@' + str(today) + '.csv')
          dfC['Date'] = [yesterday.isoformat() for i in range(dfC.shape[0])]
          dfD['Date'] = [today.isoformat() for i in range(dfD.shape[0])]
          df = pd.concat([dfC, dfD])
        else:
          df = db_production.query_execute("select DevId from "+log+" where date("+DataPre.date_column[log]+") >= '"+yesterday.isoformat()+"' and date("+DataPre.date_column[log]+") < '"+ today.isoformat()+"'")
      if not df.empty:
        count = df.pivot_table(index = ['DevId'], aggfunc = 'size')
        for dev, cou in count.iteritems():
          print(dev)
          if all([x in ["RGI", "DIAL", "BIAL", "AADSB", "BCam", "ABBA", "ABAY"] for x in dev.split('_')[:3]]) and len(dev.split('_')) == 4:
            li = list(SENSOR[dev.split('_')[0]][dev.split('_')[-2][1:]]['Device Id'])
            if dev in li:
              idd = list(SENSOR[dev.split('_')[0]][dev.split('_')[-2][1:]].columns).index(log)
              SENSOR[dev.split('_')[0]][dev.split('_')[-2][1:]].iloc[li.index(dev), idd] = cou
            else:
              print("unknown device "+dev+ " in "+log)   
          else:
            print("name error for "+dev+" in "+log)
  else:
    df = db_production.query_execute("select DevId, OperationName, FlightNo from "+log+" where date("+DataPre.date_column[log]+") >= '"+yesterday.isoformat()+"' and date("+DataPre.date_column[log]+") < '"+ today.isoformat()+"'")
    df.dropna()
    df = df[df.FlightNo.notnull()]
    arrays = [list(df['DevId']), list(df['FlightNo'])]
    tuples =list(zip(*arrays))
    index = pd.MultiIndex.from_tuples(tuples, names=['DevId', 'FlightNo'])
    df = pd.DataFrame({'op' : list(df['OperationName'])}, index = index)
    print(SENSOR)
    for dev, dev_data in df.groupby(level = 0):
      if dev.split('_')[0] not in ['RGI', 'DIAL', 'BIAL']:
        pass
      else:
        print(dev)
        if all([x in ["RGI", "DIAL", "BIAL", "AADSB", "BCam", "ABBA", "ABAY"] for x in dev.split('_')[:3]]):
          li = list(SENSOR[dev.split('_')[0]][dev.split('_')[-2][1:]]['Device Id'])
          if dev in li:
            idd = list(SENSOR[dev.split('_')[0]][dev.split('_')[-2][1:]].columns).index('Flights Served')
            iddd = list(SENSOR[dev.split('_')[0]][dev.split('_')[-2][1:]].columns).index(log)
            di = ''
            for i, row in dev_data.groupby(level = 1):
              test = pd.DataFrame(row)
              test.reset_index(inplace = True)
              SENSOR[dev.split('_')[0]][dev.split('_')[-2][1:]].iloc[li.index(dev), idd].append(i)
              zi = ''
              for inde, opr in test.iloc[:,2].iteritems():
                zi= zi + str(opr) + '|' 
              di = di + '!' + str(i) + '|' + zi
            SENSOR[dev.split('_')[0]][dev.split('_')[-2][1:]].iloc[li.index(dev), iddd] = di
          else:
            print("unknown device "+dev+ " in "+log)
        else:
          print("name error for "+dev+" in "+log)

print(RGI)

for log in data.logs:
    print(log)
    if log != 'EquipActivityLogs':
        # df1 = db_production.query_execute("select DevId, "+DataPre.date_column[log]+" from "+log+" where date("+DataPre.date_column[log]+") >= '"+daybeforedaybeforeyesterday.isoformat()+"'")
        dfA = pd.read_csv('HC/Data/EB_Data/'+ log + '@' + str(daybeforedaybeforeyesterday) + '.csv')
        dfA['Count'] = [1 for x in range(dfA.shape[0])]
        dfA = dfA.pivot_table(index=['DevId'], values = ['Count'], aggfunc='sum').reset_index()
        dfB = pd.read_csv('HC/Data/EB_Data/'+ log + '@' + str(daybeforeyesterday) + '.csv')
        dfB['Count'] = [1 for x in range(dfB.shape[0])]
        dfB = dfB.pivot_table(index=['DevId'], values = ['Count'], aggfunc='sum').reset_index()
        dfC = pd.read_csv('HC/Data/EB_Data/'+ log + '@' + str(yesterday) + '.csv')
        dfC['Count'] = [1 for x in range(dfC.shape[0])]
        dfC = dfC.pivot_table(index=['DevId'], values = ['Count'], aggfunc='sum').reset_index()
        dfD = pd.read_csv('HC/Data/EB_Data/'+ log + '@' + str(today) + '.csv')
        dfD['Count'] = [1 for x in range(dfD.shape[0])]
        dfD = dfD.pivot_table(index=['DevId'], values = ['Count'], aggfunc='sum').reset_index()
        print(dfD)
        print('---------------------------------------')
        # dfA['Date'] = [daybeforedaybeforeyesterday.isoformat() for i in range(dfA.shape[0])]
        # dfB['Date'] = [daybeforeyesterday.isoformat() for i in range(dfB.shape[0])]
        # dfC['Date'] = [yesterday.isoformat() for i in range(dfC.shape[0])]
        # dfD['Date'] = [today.isoformat() for i in range(dfD.shape[0])]
        # print('dfA_date',dfA)
        df_total = pd.concat([dfA, dfB, dfC, dfD])
        df_new = pd.concat([dfC, dfD])
        # Total_devices = list(pd.concat([dfA, dfB, dfC, dfD])['DevId'].unique())
        Total_devices = list(df_total['DevId'].unique())
        # print('concat ',df1)
        df = pd.DataFrame(columns = ['DevId', 'Count'])
        # df1 = df1.reset_index()
        # print('concat1 ',df1)
        # df1 = df1.pivot_table(index=['DevId'], values = ['Count'], aggfunc='sum')
        # print('concat1 ',df1)
        # df1 = df1.reset_index()
        # print('concat2 ',df1)
        # Total_devices = df1['DevId'].unique()
        for device in Total_devices:
            # df_device = df1[df1["DevId"] == device]
            if device in ota_devid:
                if device not in list(Without_Battery):
                    df_device = df_new[df_new["DevId"] == device]
                    df = pd.concat([df, df_device])
                    # df = pd.concat([df, df_device[df_device["Date"].astype(str).str.startswith((yesterday.isoformat()))], df_device[df_device["Date"].astype(str).str.startswith((today.isoformat()))]])
                else:
                    df_device = df_total[df_total["DevId"] == device]
                    df = pd.concat([df, df_device])
            else:
                print(device, ' is not configured but data in logs mightbe recently defaulted')
#        print(df)
        count = df.pivot_table(index=['DevId'], values = ['Count'], aggfunc='sum').reset_index()

        for idx, cou in count.iterrows():
            dev = cou['DevId']
            if dev.split('_')[0] in ['RGI', 'DIAL', 'BIAL']:
                pass
            else:
                if all([x in ["BSS", "GGI","ACAR", "ATR", "AASU", "AFL", "ALAD", "ABFL", "ACOA", "ACAT", "APBT", "AWCART", "ATCART", "AETT", "AACU", "AGPU", "AFUEL", "ABBA", "ASCHE", "ABAY", "RGI", "DIAL", "BIAL", "ASAT", "SG", "SKY", "RE", "IOSL", "BSSPL", "OFS", "CLB"] for x in dev.split('_')[:3]]):
                    li = list(vars()[str(dev.split('_')[1])][dev.split('_')[0]][dev.split('_')[2][1:]]['Device Id'])
                    if dev in li:
                        if log in list(vars()[str(dev.split('_')[1])][dev.split('_')[0]][dev.split('_')[2][1:]].columns):
                            idd = list(vars()[str(dev.split('_')[1])][dev.split('_')[0]][dev.split('_')[2][1:]].columns).index(log)
                            vars()[str(dev.split('_')[1])][dev.split('_')[0]][dev.split('_')[2][1:]].iloc[li.index(dev), idd] = cou['Count']
                            
                        else:
                            print("Extra "+log+" for "+dev)
                    else:
                        print("unknown device "+dev+ " in "+log)
                else:
                    print("name error for "+dev+" in "+log)
    else:
        df = db_production.query_execute("select DevId, OperationName, FlightNo from "+log+" where date("+DataPre.date_column[log]+") >= '"+daybeforedaybeforeyesterday.isoformat()+"'")
        df.dropna()
        df = df[df.FlightNo.notnull()]
        arrays = [list(df['DevId']), list(df['FlightNo'])]
        tuples =list(zip(*arrays))
        index = pd.MultiIndex.from_tuples(tuples, names=['DevId', 'FlightNo'])
        df = pd.DataFrame({'op' : list(df['OperationName'])}, index = index)

        for dev, dev_data in df.groupby(level = 0):
            if dev.split('_')[0] in ['RGI', 'DIAL', 'BIAL']:
                pass
            else:
                if all([x in ["BSS", "GGI", "ACAR", "ATR", "AASU", "AFL", "ALAD", "ABFL", "ACOA", "ACAT", "APBT", "AWCART", "ATCART", "AETT", "AACU", "AGPU", "AFUEL", "ABBA", "ABAY", "RGI", "DIAL", "BIAL", "ASAT", "SG", "SKY", "RE", "IOSL", "BSSPL", "OFS", "CLB"] for x in dev.split('_')[:3]]):
                    li = list(vars()[str(dev.split('_')[1])][dev.split('_')[0]][dev.split('_')[2][1:]]['Device Id'])
                    if dev in li:
                        idd = list(vars()[str(dev.split('_')[1])][dev.split('_')[0]][dev.split('_')[2][1:]].columns).index('Flights Served')
                        iddd = list(vars()[str(dev.split('_')[1])][dev.split('_')[0]][dev.split('_')[2][1:]].columns).index(log)
                        di = ''
                        for i, row in dev_data.groupby(level = 1):
                            test = pd.DataFrame(row)
                            test.reset_index(inplace = True)
                            vars()[str(dev.split('_')[1])][dev.split('_')[0]][dev.split('_')[2][1:]].iloc[li.index(dev), idd].append(i)
                            zi = ''
                            for inde, opr in test.iloc[:,2].iteritems():
                                zi= zi + str(opr) + '|' 
                            di = di + '!' + str(i) + '|' + zi
                        vars()[str(dev.split('_')[1])][dev.split('_')[0]][dev.split('_')[2][1:]].iloc[li.index(dev), iddd] = di
                    else:
                        print("unknown device "+dev+ " in "+log)
                else:
                    print("name error for "+dev+" in "+log)

json_to_csv.device_operations_to_csv('HC/Data/EB_Data/'+ 'RGI', RGI)
json_to_csv.device_logs_to_csv('HC/Data/EB_Data/'+ 'RGI', RGI)
json_to_csv.device_operations_to_csv('HC/Data/EB_Data/'+ 'DIAL', DIAL)
json_to_csv.device_logs_to_csv('HC/Data/EB_Data/'+ 'DIAL', DIAL)
json_to_csv.device_operations_to_csv('HC/Data/EB_Data/'+ 'BIAL', BIAL)
json_to_csv.device_logs_to_csv('HC/Data/EB_Data/'+ 'BIAL', BIAL)

json_to_csv.device_operations_to_csv('HC/Data/EB_Data/'+ 'SENSOR', SENSOR)
json_to_csv.device_logs_to_csv('HC/Data/EB_Data/'+ 'SENSOR', SENSOR)

if len(SENSOR['RGI']) != 0:
  RGI['Sensors'] = SENSOR['RGI']
if len(SENSOR['DIAL']) != 0:
  DIAL['Sensors'] = SENSOR['DIAL']
if len(SENSOR['BIAL']) != 0:
  BIAL['Sensors'] = SENSOR['BIAL']

TAP = []
Trails_plan = []
devicestat = extra.device_logs([RGI, DIAL, BIAL])
activity, TAP_act = extra.device_operations([RGI, DIAL, BIAL])

print(TAP_act)

print(devicestat)
print(len(devicestat))
print(activity)
print(RGI)

Flight_Arrivals = db_production.query_execute("select FlightNumber, ATA from DailyFlightScheduleArrival_GMR where OperationUnit = 4 and Date(ATA) = '"+yesterday.isoformat()+"'")
Flight_Arrival_Departures = db_production.query_execute("select FlightNumber_Arrival, ATA, Sensor_ATA, FlightNumber_Departure, ATD, Sensor_ATD from DailyFlightSchedule_Merged where OperationUnit = 4 and date(ATA) = '"+yesterday.isoformat()+"'")
Flight_Arrival_Departures = Flight_Arrival_Departures.astype(str)

Flight_Arrival_Miss = Flight_Arrival_Departures[Flight_Arrival_Departures['ATA'] == Flight_Arrival_Departures['Sensor_ATA']]
Flight_Departure_Miss = Flight_Arrival_Departures[Flight_Arrival_Departures.Sensor_ATD == 'NaT']

Flight_Arrival_Miss_Departure_Miss = Flight_Arrival_Miss[Flight_Arrival_Miss['Sensor_ATD'] == 'NaT']
Flight_Departure_Miss_Arrival_Miss = Flight_Departure_Miss[Flight_Departure_Miss['ATA'] == Flight_Departure_Miss['Sensor_ATA']]

device_stat_html = ''' <Table border = "1" style="width:100%"><Tr><Th> Airport </Th><Th> Entity </Th><Th> Type </Th><Th> Total </Th><Th>Activity Performed</Th><Th> Working </Th><Th> Down </Th></Tr>'''
st = ''
for airport in devicestat:
    count = 0
    UP = 0
    DW = 0
    for vendor in devicestat[airport]:
        count = count + len(devicestat[airport][vendor])
        for ty1 in devicestat[airport][vendor]:
            UP = UP + devicestat[airport][vendor][ty1][0]
            DW = DW + devicestat[airport][vendor][ty1][1]
    st = st + ''' <Tr><Td rowspan = '{}'> {} </br> Total devices = {} </br> Working = {}</br> Down = {} '''.format(str(count), airport, str(UP + DW), str(UP), str(DW))
    if airport == 'RGI':
        st = st + '''</br> {} Flights arrived</br> {} Flight arrivals captured</br> {} Flight departures Captured</td>'''.format(str(Flight_Arrival_Departures.shape[0]), str(Flight_Arrival_Departures.shape[0] - Flight_Arrival_Miss.shape[0]), str(Flight_Arrival_Departures.shape[0] - Flight_Departure_Miss.shape[0]))
    else:
        st = st + '''</td>'''
    tr = 0
    for vendor in devicestat[airport]:
        if tr == 1:
            st =st + '''<Tr>'''
            tr = 0
        UP = 0
        DW = 0
        for ty1 in devicestat[airport][vendor]:
            UP = UP + devicestat[airport][vendor][ty1][0]
            DW = DW + devicestat[airport][vendor][ty1][1]
        name = ''
        if vendor == 'RGI':
            name = 'Sensor'
        else:
            name = vendor
        st = st + '''<Td rowspan = '{}'> {} </br> Total devices = {} </br> Working = {}</br> Down = {} </td>'''.format(str(len(devicestat[airport][vendor])), name, str(UP + DW), str(UP), str(DW))
        for type1 in devicestat[airport][vendor]:
            tli = devicestat[airport][vendor][type1]
            if tr == 1:
                st =st + '''<Tr>'''
            st = st + '''<Td> {} </td> <Td> {} </td><td> {} </td> <Td> {} </td> <Td> {} </td>'''.format(type1, str(sum(tli)), str(activity[airport][vendor][type1][1]), str(tli[0]), str(tli[1]))
            st = st + '''</Tr>'''
            tr = 1
device_stat_html = device_stat_html + st + ''' </Table></body></html> '''
cc = ['samson@zestiot.io', 'anwesh@zestiot.io']
toaddress = ['aayush@zestiot.io', 'nilesh@zestiot.io', 'aman@zestiot.io', 'amit@zestiot.io', 'sushanto@zestiot.io', 'arshiya@zestiot.io', 'nikhileshwar@zestiot.io', 'anil@zestiot.io', 'krishna@zestiot.io', 'chiranjeevi@zestiot.io', 'rchiluka@enhops.com', 'niharika@zestiot.io'] + cc
#toaddress = ['raja@zestiot.io']
mail_to_zestiot.send_mail(DataPre.html_style + " <h2>Device health status summary</h2> " + device_stat_html, 'Health Status Summary', toaddress)



HealthMonitor = db_production.query_execute("select * from HealthMonitor")
HealthMonitor['LDate'] = HealthMonitor['LDate'].apply(lambda x : datetime.datetime.strptime(str(x), "%Y-%m-%d").strftime("%Y-%m-%d") if not isinstance(x, pd.Timestamp) else x.strftime("%Y-%m-%d"))
HealthMonitor = HealthMonitor.drop(['LogId', 'MAC', 'box_id', 'code_version', 'Restart_Reason', 'file_count', 'LogDate'], axis = 1)
HealthMonitor.columns = ['GPSLogs', 'BeaconAlerts', 'SonarLogs', 'MagneticSensorLogs', 'LDate', 'DevId', 'IgnitionLogs']
HealthMonitor['LadderLogs'] = [0 for x in list(HealthMonitor['GPSLogs'])]
HealthMonitor['MagneticSensorLogs'] = [len(x) - 2 for x in HealthMonitor['MagneticSensorLogs']]
HealthMonitor['IgnitionLogs'] = [len(x) - 1 for x in HealthMonitor['IgnitionLogs']]
HealthMonitor['FlightMovement'] = [0 for x in HealthMonitor['IgnitionLogs']]
HealthMonitor['SoundLogs'] = [0 for x in HealthMonitor['IgnitionLogs']]
HealthMonitor['BayImages'] = [0 for x in HealthMonitor['IgnitionLogs']]
HealthMonitor.to_csv('healthmonitor.csv')
print(HealthMonitor)

# "MagneticSensorLogs", "SonarLogs", "IgnitionLogs", "BeaconAlerts", "GPSLogs", "EquipActivityLogs", "LadderLogs"]
#     sensor_logs = ["EquipActivityLogs", "FlightMovement", "SoundLogs", "BayImages", "Beaco




TAP.extend(Trails_plan)
trail_plan_html = '''<Table border = "1" style="width:100%"><Tr><Th> Airport </Th><Th> Vendor </Th><Th> Type </Th><Th> Device Id </Th><Th> Missing Logs </Th><Th> Missing Act </Th><Th> Last Date </Th></Tr>'''

RGI_TAP = [x for x in TAP if x[0].split('_')[1] == 'RGI' and x[0].split('_')[-2][1:] != 'CAR']
DIAL_TAP = [x for x in TAP if x[0].split('_')[1] == 'DIAL']
BIAL_TAP = [x for x in TAP if x[0].split('_')[1] == 'BIAL']

Df_TAP_issues = pd.DataFrame(columns=['DevId', 'Log'])
Df_TAP_down = pd.DataFrame(columns=['DevId', 'Log'])


st = ''' <Td rowspan = '{}'> {}</Td>'''.format(str(len(DIAL_TAP)), 'DIAL')
RGI_vendors = list(pd.Series([x[0].split('_')[0] for x in DIAL_TAP]).unique())
for ven in RGI_vendors:
  ven_li = [x for x in DIAL_TAP if x[0].split('_')[0] == ven]
  st = st + ''' <Td rowspan = '{}'> {}</Td>'''.format(str(len(ven_li)), ven)
  RGI_types = list(pd.Series([x[0].split('_')[-2][1:] for x in ven_li]).unique())
  for typ in RGI_types:
    typ_li = [x for x in ven_li if x[0].split('_')[-2][1:] == typ]
    st = st + ''' <Td rowspan = '{}'> {}</Td>'''.format(str(len(typ_li)), typ)
    typ_li_miss = [x for x in typ_li if x[1] != 'No Logs' and x[1] != 'Working']
    typ_li_down = [x for x in typ_li if x[1] == 'No Logs']
    typ_li_Work = [x for x in typ_li if x[1] == 'Working']
  
    for device_log in typ_li_Work:
      lastdate = 'empty'
      logs_req = DataPre.lo[device_log[0].split('_')[2][1:]]
      if device_log[0].split('_')[2][1:] == 'LAD':
        for idlog in range(len(logs_req)):
            if 'EquipActivityLogs' in logs_req[idlog]:
                logs_req[idlog].remove('EquipActivityLogs')
      else:
        if 'EquipActivityLogs' in logs_req:
            logs_req.remove('EquipActivityLogs')
      HealthMonitor_temp = HealthMonitor[HealthMonitor['DevId'] == device_log[0]]
      HealthMonitor_temp = HealthMonitor_temp.sort_values('LDate', ascending = False)
      for idx, row in HealthMonitor_temp.iterrows():
        if device_log[0].split('_')[2][1:] != 'LAD':
            if all([True if int(row[x]) > 0 else False for x in logs_req]):
                lastdate = row['LDate']
                break
        else:
            if any([all([True if int(row[x]) > 0 else False for x in y]) for y in logs_req]):
                lastdate = row['LDate']
                break
      if lastdate == today.isoformat():
          codlor = "greenyellow"
      elif lastdate == yesterday.isoformat() or lastdate == daybeforeyesterday.isoformat() or lastdate == daybeforedaybeforeyesterday.isoformat():
          codlor = "goldenrod"
      else:
          codlor = "lightcoral"

      if TAP_act[str(device_log[0])][1] == 'Working':
          st = st + '''<Td bgcolor = "greenyellow"> {} </Td><Td> {} </Td><Td bgcolor = "greenyellow"> {} </Td><Td bgcolor = {}> {} </Td>'''.format(str(device_log[0]), str(','.join(device_log[1:])), TAP_act[str(device_log[0])][0], codlor, lastdate)
      elif TAP_act[str(device_log[0])][1] == 'Down':
          st = st + '''<Td bgcolor = "greenyellow"> {} </Td><Td> {} </Td><Td bgcolor = "lightcoral"> {} </Td><Td bgcolor = {}> {} </Td>'''.format(str(device_log[0]), str(','.join(device_log[1:])), TAP_act[str(device_log[0])][0], codlor, lastdate)
      st = st + '''</Tr><Tr>''' 
    for device_log in typ_li_miss:
      lastdate = 'empty'
      logs_req = DataPre.lo[device_log[0].split('_')[2][1:]]
      if device_log[0].split('_')[2][1:] == 'LAD':
        for idlog in range(len(logs_req)):
            if 'EquipActivityLogs' in logs_req[idlog]:
                logs_req[idlog].remove('EquipActivityLogs')
      else:
        if 'EquipActivityLogs' in logs_req:
            logs_req.remove('EquipActivityLogs')
      HealthMonitor_temp = HealthMonitor[HealthMonitor['DevId'] == device_log[0]]
      HealthMonitor_temp = HealthMonitor_temp.sort_values('LDate', ascending = False)
      for idx, row in HealthMonitor_temp.iterrows():
        if device_log[0].split('_')[2][1:] != 'LAD':
            if all([True if int(row[x]) > 0 else False for x in logs_req]):
                lastdate = row['LDate']
                break
        else:
            if any([all([True if int(row[x]) > 0 else False for x in y]) for y in logs_req]):
                lastdate = row['LDate']
                break
      if lastdate == today.isoformat():
          codlor = "greenyellow"
      elif lastdate == yesterday.isoformat() or lastdate == daybeforeyesterday.isoformat() or lastdate == daybeforedaybeforeyesterday.isoformat():
          codlor = "goldenrod"
      else:
          codlor = "lightcoral"
      # st = st + '''<Td bgcolor = "goldenrod"> {} </Td><Td> {} </Td><Td> {} </Td>'''.format(str(device_log[0]), str(','.join(device_log[1:])), lastdate)
      if TAP_act[str(device_log[0])][1] == 'Working':
          st = st + '''<Td bgcolor = "goldenrod"> {} </Td><Td> {} </Td><Td bgcolor = "greenyellow"> {} </Td><Td bgcolor = {}> {} </Td>'''.format(str(device_log[0]), str(','.join(device_log[1:])), TAP_act[str(device_log[0])][0], codlor, lastdate)
      elif TAP_act[str(device_log[0])][1] == 'Down':
          st = st + '''<Td bgcolor = "goldenrod"> {} </Td><Td> {} </Td><Td bgcolor = "lightcoral"> {} </Td><Td bgcolor = {}> {} </Td>'''.format(str(device_log[0]), str(','.join(device_log[1:])), TAP_act[str(device_log[0])][0], codlor, lastdate)
      Df_TAP_issues = pd.concat([Df_TAP_issues, pd.DataFrame({'DevId' : [str(device_log[0])], 'Log' :[str('Missing ' + ','.join(device_log[1:]))]})])
      st = st + '''</Tr><Tr>''' 
    for device_log in typ_li_down:
      lastdate = 'empty'
      logs_req = DataPre.lo[device_log[0].split('_')[2][1:]]
      if device_log[0].split('_')[2][1:] == 'LAD':
        for idlog in range(len(logs_req)):
            if 'EquipActivityLogs' in logs_req[idlog]:
                logs_req[idlog].remove('EquipActivityLogs')
      else:
        if 'EquipActivityLogs' in logs_req:
            logs_req.remove('EquipActivityLogs')
      HealthMonitor_temp = HealthMonitor[HealthMonitor['DevId'] == device_log[0]]
      HealthMonitor_temp = HealthMonitor_temp.sort_values('LDate', ascending = False)
      for idx, row in HealthMonitor_temp.iterrows():
        if device_log[0].split('_')[2][1:] != 'LAD':
            if all([True if int(row[x]) > 0 else False for x in logs_req]):
                lastdate = row['LDate']
                break
        else:
            if any([all([True if int(row[x]) > 0 else False for x in y]) for y in logs_req]):
                lastdate = row['LDate']
                break
      if lastdate == today.isoformat():
          codlor = "greenyellow"
      elif lastdate == yesterday.isoformat() or lastdate == daybeforeyesterday.isoformat() or lastdate == daybeforedaybeforeyesterday.isoformat():
          codlor = "goldenrod"
      else:
          codlor = "lightcoral"
      # st = st + '''<Td bgcolor = "lightcoral"> {} </Td><Td> {} </Td><Td> {} </Td>'''.format(str(device_log[0]), str(','.join(device_log[1:])), lastdate)
      if TAP_act[str(device_log[0])][1] == 'Working':
          st = st + '''<Td bgcolor = "lightcoral"> {} </Td><Td> {} </Td><Td bgcolor = "greenyellow"> {} </Td><Td bgcolor = {}> {} </Td>'''.format(str(device_log[0]), str(','.join(device_log[1:])), TAP_act[str(device_log[0])][0], codlor, lastdate)
      elif TAP_act[str(device_log[0])][1] == 'Down':
          st = st + '''<Td bgcolor = "lightcoral"> {} </Td><Td> {} </Td><Td bgcolor = "lightcoral"> {} </Td><Td bgcolor = {}> {} </Td>'''.format(str(device_log[0]), str(','.join(device_log[1:])), TAP_act[str(device_log[0])][0], codlor, lastdate)
      Df_TAP_down = pd.concat([Df_TAP_down, pd.DataFrame({'DevId' : [str(device_log[0])], 'Log' :[str(','.join(device_log[1:]) + ' Found')]})])
      st = st + '''</Tr><Tr>''' 

st = st + ''' <Tr><Td rowspan = '{}'> {}</Td>'''.format(str(len(RGI_TAP)), 'RGI')

RGI_vendors = list(pd.Series([x[0].split('_')[0] for x in RGI_TAP]).unique())
if 'SG' in RGI_vendors:
  RGI_vendors.remove('SG')
  RGI_vendors.insert(0, 'SG')
for ven in RGI_vendors:
  ven_li = [x for x in RGI_TAP if x[0].split('_')[0] == ven]
  st = st + ''' <Td rowspan = '{}'> {}</Td>'''.format(str(len(ven_li)), ven)
  RGI_types = list(pd.Series([x[0].split('_')[-2][1:] for x in ven_li]).unique())
  for typ in RGI_types:
    typ_li = [x for x in ven_li if x[0].split('_')[-2][1:] == typ]
    st = st + ''' <Td rowspan = '{}'> {}</Td>'''.format(str(len(typ_li)), typ)
    typ_li_miss = [x for x in typ_li if x[1] != 'No Logs' and x[1] != 'Working']
    typ_li_down = [x for x in typ_li if x[1] == 'No Logs']
    typ_li_Work = [x for x in typ_li if x[1] == 'Working']
  
    for device_log in typ_li_Work:
      lastdate = 'empty'
      logs_req = DataPre.lo[device_log[0].split('_')[2][1:]]
      if device_log[0].split('_')[2][1:] == 'LAD':
        for idlog in range(len(logs_req)):
            if 'EquipActivityLogs' in logs_req[idlog]:
                logs_req[idlog].remove('EquipActivityLogs')
      else:
        if 'EquipActivityLogs' in logs_req:
            logs_req.remove('EquipActivityLogs')
      HealthMonitor_temp = HealthMonitor[HealthMonitor['DevId'] == device_log[0]]
      HealthMonitor_temp = HealthMonitor_temp.sort_values('LDate', ascending = False)
      for idx, row in HealthMonitor_temp.iterrows():
        if device_log[0].split('_')[2][1:] != 'LAD':
            if all([True if int(row[x]) > 0 else False for x in logs_req]):
                lastdate = row['LDate']
                break
        else:
            if any([all([True if int(row[x]) > 0 else False for x in y]) for y in logs_req]):
                lastdate = row['LDate']
                break

      if lastdate == today.isoformat():
          codlor = "greenyellow"
      elif lastdate == yesterday.isoformat() or lastdate == daybeforeyesterday.isoformat() or lastdate == daybeforedaybeforeyesterday.isoformat():
          codlor = "goldenrod"
      else:
          codlor = "lightcoral"
      # st = st + '''<Td bgcolor = "greenyellow"> {} </Td><Td> {} </Td><Td> {} </Td>'''.format(str(device_log[0]), str(','.join(device_log[1:])), lastdate)
      if TAP_act[str(device_log[0])][1] == 'Working':
          st = st + '''<Td bgcolor = "greenyellow"> {} </Td><Td> {} </Td><Td bgcolor = "greenyellow"> {} </Td><Td bgcolor = {}> {} </Td>'''.format(str(device_log[0]), str(','.join(device_log[1:])), TAP_act[str(device_log[0])][0], codlor, lastdate)
      elif TAP_act[str(device_log[0])][1] == 'Down':
          st = st + '''<Td bgcolor = "greenyellow"> {} </Td><Td> {} </Td><Td bgcolor = "lightcoral"> {} </Td><Td bgcolor = {}> {} </Td>'''.format(str(device_log[0]), str(','.join(device_log[1:])), TAP_act[str(device_log[0])][0], codlor, lastdate)
      st = st + '''</Tr><Tr>''' 
    for device_log in typ_li_miss:
      lastdate = 'empty'
      logs_req = DataPre.lo[device_log[0].split('_')[2][1:]]
      if device_log[0].split('_')[2][1:] == 'LAD':
        for idlog in range(len(logs_req)):
            if 'EquipActivityLogs' in logs_req[idlog]:
                logs_req[idlog].remove('EquipActivityLogs')
      else:
        if 'EquipActivityLogs' in logs_req:
            logs_req.remove('EquipActivityLogs')
      HealthMonitor_temp = HealthMonitor[HealthMonitor['DevId'] == device_log[0]]
      HealthMonitor_temp = HealthMonitor_temp.sort_values('LDate', ascending = False)
      for idx, row in HealthMonitor_temp.iterrows():
        if device_log[0].split('_')[2][1:] !='LAD':
            if all([True if int(row[x]) > 0 else False for x in logs_req]):
                lastdate = row['LDate']
                break
        else:
            if any([all([True if int(row[x]) > 0 else False for x in y]) for y in logs_req]):
                lastdate = row['LDate']
                break
      if lastdate == today.isoformat():
          codlor = "greenyellow"
      elif lastdate == yesterday.isoformat() or lastdate == daybeforeyesterday.isoformat() or lastdate == daybeforedaybeforeyesterday.isoformat():
          codlor = "goldenrod"
      else:
          codlor = "lightcoral"
      # st = st + '''<Td bgcolor = "goldenrod"> {} </Td><Td> {} </Td><Td> {} </Td>'''.format(str(device_log[0]), str(','.join(device_log[1:])), lastdate)
      if TAP_act[str(device_log[0])][1] == 'Working':
          st = st + '''<Td bgcolor = "goldenrod"> {} </Td><Td> {} </Td><Td bgcolor = "greenyellow"> {} </Td><Td bgcolor = {}> {} </Td>'''.format(str(device_log[0]), str(','.join(device_log[1:])), TAP_act[str(device_log[0])][0], codlor, lastdate)
      elif TAP_act[str(device_log[0])][1] == 'Down':
          st = st + '''<Td bgcolor = "goldenrod"> {} </Td><Td> {} </Td><Td bgcolor = "lightcoral"> {} </Td><Td bgcolor = {}> {} </Td>'''.format(str(device_log[0]), str(','.join(device_log[1:])), TAP_act[str(device_log[0])][0], codlor, lastdate)
      Df_TAP_issues = pd.concat([Df_TAP_issues, pd.DataFrame({'DevId' : [str(device_log[0])], 'Log' :[str('Missing ' + ','.join(device_log[1:]))]})])
      st = st + '''</Tr><Tr>''' 
    for device_log in typ_li_down:
      lastdate = 'empty'
      logs_req = DataPre.lo[device_log[0].split('_')[2][1:]]
      if device_log[0].split('_')[2][1:] == 'LAD':
        for idlog in range(len(logs_req)):
            if 'EquipActivityLogs' in logs_req[idlog]:
                logs_req[idlog].remove('EquipActivityLogs')
      else:
        if 'EquipActivityLogs' in logs_req:
            logs_req.remove('EquipActivityLogs')
      HealthMonitor_temp = HealthMonitor[HealthMonitor['DevId'] == device_log[0]]
      HealthMonitor_temp = HealthMonitor_temp.sort_values('LDate', ascending = False)
      for idx, row in HealthMonitor_temp.iterrows():
        if device_log[0].split('_')[2][1:] != 'LAD':
            if all([True if int(row[x]) > 0 else False for x in logs_req]):
                lastdate = row['LDate']
                break
        else:
            if any([all([True if int(row[x]) > 0 else False for x in y]) for y in logs_req]):
                lastdate = row['LDate']
                break
      if lastdate == today.isoformat():
          codlor = "greenyellow"
      elif lastdate == yesterday.isoformat() or lastdate == daybeforeyesterday.isoformat() or lastdate == daybeforedaybeforeyesterday.isoformat():
          codlor = "goldenrod"
      else:
          codlor = "lightcoral"
      # st = st + '''<Td bgcolor = "lightcoral"> {} </Td><Td> {} </Td><Td> {} </Td>'''.format(str(device_log[0]), str(','.join(device_log[1:])), lastdate)
      if TAP_act[str(device_log[0])][1] == 'Working':
          st = st + '''<Td bgcolor = "lightcoral"> {} </Td><Td> {} </Td><Td bgcolor = "greenyellow"> {} </Td><Td bgcolor = {}> {} </Td>'''.format(str(device_log[0]), str(','.join(device_log[1:])), TAP_act[str(device_log[0])][0], codlor, lastdate)
      elif TAP_act[str(device_log[0])][1] == 'Down':
          st = st + '''<Td bgcolor = "lightcoral"> {} </Td><Td> {} </Td><Td bgcolor = "lightcoral"> {} </Td><Td bgcolor = {}> {} </Td>'''.format(str(device_log[0]), str(','.join(device_log[1:])), TAP_act[str(device_log[0])][0], codlor, lastdate)
      Df_TAP_down = pd.concat([Df_TAP_down, pd.DataFrame({'DevId' : [str(device_log[0])], 'Log' :[str(','.join(device_log[1:]) + ' Found')]})])
      st = st + '''</Tr><Tr>'''       
        
# st = st + ''' <Td rowspan = '{}'> {}</Td>'''.format(str(len(DIAL_TAP)), 'DIAL')
# RGI_vendors = list(pd.Series([x[0].split('_')[0] for x in DIAL_TAP]).unique())
# for ven in RGI_vendors:
#   ven_li = [x for x in DIAL_TAP if x[0].split('_')[0] == ven]
#   st = st + ''' <Td rowspan = '{}'> {}</Td>'''.format(str(len(ven_li)), ven)
#   RGI_types = list(pd.Series([x[0].split('_')[-2][1:] for x in ven_li]).unique())
#   for typ in RGI_types:
#     typ_li = [x for x in ven_li if x[0].split('_')[-2][1:] == typ]
#     st = st + ''' <Td rowspan = '{}'> {}</Td>'''.format(str(len(typ_li)), typ)
#     typ_li_miss = [x for x in typ_li if x[1] != 'No Logs' and x[1] != 'Working']
#     typ_li_down = [x for x in typ_li if x[1] == 'No Logs']
#     typ_li_Work = [x for x in typ_li if x[1] == 'Working']
  
#     for device_log in typ_li_Work:
#       st = st + '''<Td bgcolor = "greenyellow"> {} </Td><Td> {} </Td>'''.format(str(device_log[0]), str(','.join(device_log[1:])))
#       st = st + '''</Tr><Tr>''' 
#     for device_log in typ_li_miss:
#       st = st + '''<Td bgcolor = "goldenrod"> {} </Td><Td> {} </Td>'''.format(str(device_log[0]), str(','.join(device_log[1:])))
#       Df_TAP_issues = pd.concat([Df_TAP_issues, pd.DataFrame({'DevId' : [str(device_log[0])], 'Log' :[str('Missing ' + ','.join(device_log[1:]))]})])
#       st = st + '''</Tr><Tr>''' 
#     for device_log in typ_li_down:
#       st = st + '''<Td bgcolor = "lightcoral"> {} </Td><Td> {} </Td>'''.format(str(device_log[0]), str(','.join(device_log[1:])))
#       Df_TAP_down = pd.concat([Df_TAP_down, pd.DataFrame({'DevId' : [str(device_log[0])], 'Log' :[str(','.join(device_log[1:]) + ' Found')]})])
#       st = st + '''</Tr><Tr>''' 

st = st + '''<Td rowspan = '{}'> {}</Td>'''.format(str(len(BIAL_TAP)), 'BIAL')
RGI_vendors = list(pd.Series([x[0].split('_')[0] for x in BIAL_TAP]).unique())
for ven in RGI_vendors:
  ven_li = [x for x in BIAL_TAP if x[0].split('_')[0] == ven]
  st = st + ''' <Td rowspan = '{}'> {}</Td>'''.format(str(len(ven_li)), ven)
  RGI_types = list(pd.Series([x[0].split('_')[-2][1:] for x in ven_li]).unique())
  for typ in RGI_types:
    typ_li = [x for x in ven_li if x[0].split('_')[-2][1:] == typ]
    st = st + ''' <Td rowspan = '{}'> {}</Td>'''.format(str(len(typ_li)), typ)
    typ_li_miss = [x for x in typ_li if x[1] != 'No Logs'and x[1] != 'Working']
    typ_li_down = [x for x in typ_li if x[1] == 'No Logs']
    typ_li_Work = [x for x in typ_li if x[1] == 'Working']
  
    for device_log in typ_li_Work:
      
      lastdate = 'empty'
      logs_req = DataPre.lo[device_log[0].split('_')[2][1:]]
      if device_log[0].split('_')[2][1:] == 'LAD':
        for idlog in range(len(logs_req)):
            if 'EquipActivityLogs' in logs_req[idlog]:
                logs_req[idlog].remove('EquipActivityLogs')
      else:
        if 'EquipActivityLogs' in logs_req:
            logs_req.remove('EquipActivityLogs')
      
      HealthMonitor_temp = HealthMonitor[HealthMonitor['DevId'] == device_log[0]]
      HealthMonitor_temp = HealthMonitor_temp.sort_values('LDate', ascending = False)
      for idx, row in HealthMonitor_temp.iterrows():
        if device_log[0].split('_')[2][1:] != 'LAD':
            if all([True if int(row[x]) > 0 else False for x in logs_req]):
                lastdate = row['LDate']
                break
        else:
            if any([all([True if int(row[x]) > 0 else False for x in y]) for y in logs_req]):
                lastdate = row['LDate']
                break
      if lastdate == today.isoformat():
          codlor = "greenyellow"
      elif lastdate == yesterday.isoformat() or lastdate == daybeforeyesterday.isoformat() or lastdate == daybeforedaybeforeyesterday.isoformat():
          codlor = "goldenrod"
      else:
          codlor = "lightcoral"
      # st = st + '''<Td bgcolor = "greenyellow"> {} </Td><Td> {} </Td><Td> {} </Td>'''.format(str(device_log[0]), str(','.join(device_log[1:])), lastdate)
      if TAP_act[str(device_log[0])][1] == 'Working':
          st = st + '''<Td bgcolor = "greenyellow"> {} </Td><Td> {} </Td><Td bgcolor = "greenyellow"> {} </Td><Td bgcolor = {}> {} </Td>'''.format(str(device_log[0]), str(','.join(device_log[1:])), TAP_act[str(device_log[0])][0], codlor, lastdate)
      elif TAP_act[str(device_log[0])][1] == 'Down':
          st = st + '''<Td bgcolor = "greenyellow"> {} </Td><Td> {} </Td><Td bgcolor = "lightcoral"> {} </Td><Td bgcolor = {}> {} </Td>'''.format(str(device_log[0]), str(','.join(device_log[1:])), TAP_act[str(device_log[0])][0], codlor, lastdate)
      st = st + '''</Tr><Tr>''' 
    for device_log in typ_li_miss:
      lastdate = 'empty'
      logs_req = DataPre.lo[device_log[0].split('_')[2][1:]]
      if device_log[0].split('_')[2][1:] == 'LAD':
        for idlog in range(len(logs_req)):
            if 'EquipActivityLogs' in logs_req[idlog]:
                logs_req[idlog].remove('EquipActivityLogs')
      else:
        if 'EquipActivityLogs' in logs_req:
            logs_req.remove('EquipActivityLogs')
      HealthMonitor_temp = HealthMonitor[HealthMonitor['DevId'] == device_log[0]]
      HealthMonitor_temp = HealthMonitor_temp.sort_values('LDate', ascending = False)
      for idx, row in HealthMonitor_temp.iterrows():
        if device_log[0].split('_')[2][1:] != 'LAD':
            if all([True if int(row[x]) > 0 else False for x in logs_req]):
                lastdate = row['LDate']
                break
        else:
            if any([all([True if int(row[x]) > 0 else False for x in y]) for y in logs_req]):
                lastdate = row['LDate']
                break
      if lastdate == today.isoformat():
          codlor = "greenyellow"
      elif lastdate == yesterday.isoformat() or lastdate == daybeforeyesterday.isoformat() or lastdate == daybeforedaybeforeyesterday.isoformat():
          codlor = "goldenrod"
      else:
          codlor = "lightcoral"
      # st = st + '''<Td bgcolor = "goldenrod"> {} </Td><Td> {} </Td><Td> {} </Td>'''.format(str(device_log[0]), str(','.join(device_log[1:])), lastdate)
      if TAP_act[str(device_log[0])][1] == 'Working':
          st = st + '''<Td bgcolor = "goldenrod"> {} </Td><Td> {} </Td><Td bgcolor = "greenyellow"> {} </Td><Td bgcolor = {}> {} </Td>'''.format(str(device_log[0]), str(','.join(device_log[1:])), TAP_act[str(device_log[0])][0], codlor, lastdate)
      elif TAP_act[str(device_log[0])][1] == 'Down':
          st = st + '''<Td bgcolor = "goldenrod"> {} </Td><Td> {} </Td><Td bgcolor = "lightcoral"> {} </Td><Td bgcolor = {}> {} </Td>'''.format(str(device_log[0]), str(','.join(device_log[1:])), TAP_act[str(device_log[0])][0], codlor, lastdate)
      Df_TAP_issues = pd.concat([Df_TAP_issues, pd.DataFrame({'DevId' : [str(device_log[0])], 'Log' :[str('Missing ' + ','.join(device_log[1:]))]})])
      st = st + '''</Tr><Tr>''' 
    for device_log in typ_li_down:
      print(device_log[0])
      lastdate = 'empty'
      logs_req = DataPre.lo[device_log[0].split('_')[2][1:]]
      if device_log[0].split('_')[2][1:] == 'LAD':
        for idlog in range(len(logs_req)):
            if 'EquipActivityLogs' in logs_req[idlog]:
                logs_req[idlog].remove('EquipActivityLogs')
      else:
        if 'EquipActivityLogs' in logs_req:
            logs_req.remove('EquipActivityLogs')
      print(logs_req)
      
      HealthMonitor_temp = HealthMonitor[HealthMonitor['DevId'] == device_log[0]]
      HealthMonitor_temp = HealthMonitor_temp.sort_values('LDate', ascending = False)
      for idx, row in HealthMonitor_temp.iterrows():
        # print(row)
        if device_log[0].split('_')[2][1:] != 'LAD':
            print([True if int(row[x]) > 0 else False for x in logs_req])
            if all([True if int(row[x]) > 0 else False for x in logs_req]):
                lastdate = row['LDate']
                break
        else:
            if any([all([True if int(row[x]) > 0 else False for x in y]) for y in logs_req]):
                lastdate = row['LDate']
                break
      if lastdate == today.isoformat():
          codlor = "greenyellow"
      elif lastdate == yesterday.isoformat() or lastdate == daybeforeyesterday.isoformat() or lastdate == daybeforedaybeforeyesterday.isoformat():
          codlor = "goldenrod"
      else:
          codlor = "lightcoral"
      # st = st + '''<Td bgcolor = "lightcoral"> {} </Td><Td> {} </Td><Td> {} </Td>'''.format(str(device_log[0]), str(','.join(device_log[1:])), lastdate)
      if TAP_act[str(device_log[0])][1] == 'Working':
          st = st + '''<Td bgcolor = "lightcoral"> {} </Td><Td> {} </Td><Td bgcolor = "greenyellow"> {} </Td><Td bgcolor = {}> {} </Td>'''.format(str(device_log[0]), str(','.join(device_log[1:])), TAP_act[str(device_log[0])][0], codlor, lastdate)
      elif TAP_act[str(device_log[0])][1] == 'Down':
          st = st + '''<Td bgcolor = "lightcoral"> {} </Td><Td> {} </Td><Td bgcolor = "lightcoral"> {} </Td><Td bgcolor = {}> {} </Td>'''.format(str(device_log[0]), str(','.join(device_log[1:])), TAP_act[str(device_log[0])][0], codlor, lastdate)
      Df_TAP_down = pd.concat([Df_TAP_down, pd.DataFrame({'DevId' : [str(device_log[0])], 'Log' :[str(','.join(device_log[1:]) + ' Found')]})])
      st = st + '''</Tr><Tr>'''   
st = st[:-5] 
trail_plan_html = trail_plan_html + st + ''' </Table> </body> </html>'''

Df_TAP_issues.to_csv('HC/Data/EB_Data/'+ 'Issues.csv')
Df_TAP_down.to_csv('HC/Data/EB_Data/'+ 'Down.csv')
cc = ['samson@zestiot.io', 'anwesh@zestiot.io', 'syed@zestiot.io', 'arshad@zestiot.io']
toaddress = ['aayush@zestiot.io', 'nilesh@zestiot.io', 'aman@zestiot.io', 'amit@zestiot.io', 'sushanto@zestiot.io', 'arshiya@zestiot.io', 'nikhileshwar@zestiot.io', 'anil@zestiot.io', 'krishna@zestiot.io', 'chiranjeevi@zestiot.io', 'rchiluka@enhops.com', 'niharika@zestiot.io'] + cc
#toaddress = ['raja@zestiot.io']
mail_to_zestiot.send_mail(DataPre.html_style + " <h2>Troubleshoot and Trails Plan</h2> " + trail_plan_html, 'Health Status Troubleshoot Plan', toaddress)

print(len(TAP))
print(TAP)

fo = open('HC/Data/EB_Data/'+ 'Results' + today.isoformat() +'.txt', 'w')
for device_list in TAP:
    for element in device_list:
        fo.write(element + ',')
    fo.write('\n')
fo.close()
